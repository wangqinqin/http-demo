" vim: set filetype=vim foldmethod=marker:

" General{{{

" No compatible
se nocp                 " nocp = no compatible

" Encodings
se enc=utf-8            " enc = encoding
se tenc=utf-8           " tenc = termencoding
se fenc=utf-8           " fenc =filencoding
se fencs=ucs-bom,utf-8,cp936,gb18030,big5,latin1    " fencs = fileencodings

" With a map leader it's possible to do extra key combinations
let mapleader = ","
let g:mapleader = ","

" When .vimrc is edited, reload it
au! bufwritepost .vimrc source $MYVIMRC

" Fast editing .vimrc
nm <leader>e :e $MYVIMRC<cr>
" Fast saving
nm <leader>w :w!<cr>

" Set to <ESC>
ino jk <ESC>

" Set te to :tabe
nn te :tabe<Enter>

" yy copy to system clipboard
se cb=unnamed           " cb = clipboard

" Set auto-formating better for Chinese
se fo+=mM               " fo = formatoptions

" Set textwidth to 78 for text files
" au! bufNewFile,bufReadPost *.txt se tw=78   " tw = textwidth

" Map Q to gq
map Q gq

" Set SPACE to scroll down
nn <Space> <C-d>

" Set paste toggle key
se pt=<F10>             " pt = pastetoggle

" Auto change to current directory
se acd                  " acd = autochdir

" Chinese help language
se hlg=cn               " hlg = helplang

filetype plugin indent on   " REQUIRED!
"}}}

" Interface{{{

" Set font for Gui/MacVim
" se gfn=Menlo\ for\ Powerline:h16    " gfn = guifont

" Show line numbers
se nu                   " nu = number

" Show commands
se sc                   " sc = show commands

" Show magic
se magic

" Dynamic title
se title

" Show ruler
se ru                   " ru = ruler

" Show column at line 80
se cc=80                " cc = colorcolumn

" Show mode
se smd                  " smd = show mode
se wmnu                 " wmnu = wildmenu

se bs=eol,start,indent  " bs = backspace

" linebreak
" not good for Chinese
se lbr

" Change the status line color
" http://vim.wikia.com/wiki/Change_statusline_color_to_show_insert_or_normal_mode
" Always show status bar
se ls=2                 " ls is laststatus

" 显示十六进制字符
:set statusline=%<%f%h%m%r%=%b\ 0x%B\ \ %l,%c%V\ %P


" Highlight searching
se hls                  " hls = hlsearch
hi Visual  guifg=#000000 guibg=#FFFFFF gui=none
" Increase searching
se is                   " is = incsearch
" Search show next match string at the center of screen
nn n nzz
nn N Nzz

" Show blank symbols when :se list
se lcs=nbsp:¬,eol:¶,tab:>-,extends:»,precedes:«,trail:•

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

se aw                   " autowrite
"}}}

" Indent & filetypes{{{

" 自动缩进
se ai                   " ai = autoindent
" 智能缩进
se si                   " si = smartindent
" C/C++ 风格缩进
se cin                  " cin = cindent

" 默认使用 4 空格缩进
se et                   " expandtab
se sta                  " smarttab
se sw=4                 " shiftwidth
se ts=4                 " tabstop
se sts=4                " softtabstop

" use speicified indent on different filetypes " why setlocal?
au FileType debcontrol  se et sw=1 ts=1 sts=1
au FileType ruby        se et sw=2 ts=2 sts=2
au FileType html        se et sw=2 ts=2 sts=2
au FileType yaml,yml    se et sw=2 ts=2 sts=2
au BufEnter *.tmpl      se et sw=2 ts=2 sts=2
au FileType markdown    se et sw=4 ts=4 sts=4

" do not expand tab in Makefiles
au FileType make,go             se noet

" fix crontab: temp file must be edited in place
autocmd Filetype crontab        setlocal nobackup nowritebackup
autocmd Filetype gitcommit      se spell textwidth=72 cc=72

" use perl sytax highlight on Rexfile
au BufRead,BufWrite *Rexfile    se ft=perl

" disable sh ft on *.install files
au BufRead,BufWrite *.install   se ft=install

" auto remove trailing spaces when save
" https://unix.stackexchange.com/questions/75430/how-to-automatically-strip-trailing-spaces-on-save-in-vi-and-vim
autocmd BufWritePre * :%s/\s\+$//e

" auto remove blank lines at EOF when save
" https://stackoverflow.com/questions/7495932/how-can-i-trim-blank-lines-at-the-end-of-file-in-vim
autocmd BufWritePre * :%s#\($\n\s*\)\+\%$##e
"}}}

" Syntax{{{

syn enable
syn on
"}}}

" Alias{{{

iab idate <c-r>=strftime("%Y-%m-%d")<CR>
iab itime <c-r>=strftime("%H:%M")<CR>
iab imail pityonline <pityonline@gmail.com>
iab igmail pityonline@gmail.com
iab idrop >/dev/null 2>&1
iab """"" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
iab ##### #############################################################################
iab ===== =============================================================================
iab ***** *****************************************************************************
"}}}

" Windows Management{{{

nn wn <C-w>n            " new split window vertical
nn wv <C-w>v            " copy horizontal
nn wh <C-w>h            " mv left
nn wj <C-w>j            " mv down
nn wk <C-w>k            " mv up
nn wl <C-w>l            " mv right
nn wt <C-w>t            " mv top
nn wb <C-w>b            " mv bottom
nn wT <C-w>T            " mv out to tab
nn wx <C-w>x            " exchange
nn wH <C-w>H            " turn vertical to horizontal then mv left
nn wJ <C-w>J            " turn horizontal to vertical then mv down
nn wK <C-w>K            " turn horizontal to vertical then mv up
nn wL <C-w>L            " turn vertical to horizontal then mv right
nn wc <C-w>c<cr>        " close window
nn wo :vne<cr>          " new split window horizontal

" Set Up/Down non-linewise
no <Up> gk
no <Down> gj
"}}}

" Plugins{{{

" NERDTree{{{2

no <silent> <leader>n :NERDTreeToggle<CR>
"}}}

" Syntastic{{{2
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0   " 不要每次都自动打开 quickfix，因为关不掉
let g:syntastic_check_on_open = 1

let g:syntastic_go_checkers = ['gometalinter']
let g:syntastic_go_gometalinter_args = ['--disable-all', '--enable=vet', '--enable=golint']

" 忽略一些文件类型的检查，如某个目录中的文件，或以某种扩展名结尾的文件
" let g:syntastic_ignore_files = ['\m^/usr/include/', '\m\c\.xml']

let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "active_filetypes": [],
    \ "passive_filetypes": ["xml"]
    \}

" shell
let g:syntastic_sh_shellcheck_args = "-e SC1117"

" python
let g:syntastic_python_checkers = ['flake8', 'pylint']
" let g:syntastic_python_pylint_args="-d msg1,msg2"
let g:syntastic_python_flake8_args='--ignore=E302,E305,E501'
"}}}

" vim-go{{{2

let g:go_list_type                   = "quickfix"
let g:go_fmt_autosave                = 1
let g:go_fmt_command                 = "goimports"
let g:go_test_timeout                = '10s'    " default to 10 seconds

" highlight
let g:go_highlight_types             = 1
let g:go_highlight_fields            = 1
let g:go_highlight_functions         = 1
let g:go_highlight_methods           = 1
let g:go_highlight_operators         = 1
let g:go_highlight_extra_types       = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_generate_tags     = 1

" metalinter
let g:go_metalinter_enabled          = ['vet', 'golint', 'errcheck']
let g:go_metalinter_autosave         = 1
let g:go_metalinter_autosave_enabled = ['vet', 'golint']
let g:go_metalinter_deadline         = "5s"

let g:go_auto_type_info              = 1
let g:go_auto_sameids                = 1

autocmd FileType go nmap <leader>B  <Plug>(go-build)
autocmd FileType go nmap <leader>R  <Plug>(go-run)
autocmd FileType go nmap <leader>T  <Plug>(go-test)
autocmd FileType go nmap <Leader>C  <Plug>(go-coverage-toggle)
autocmd FileType go nmap <Leader>i  <Plug>(go-info)

autocmd Filetype go command! -bang A  call go#alternate#Switch(<bang>0, 'edit')
autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
autocmd Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
"}}}

" tagbar{{{2

nn <silent> <leader>t :TagbarToggle<CR>
" au VimEnter * nested :TagbarOpen

" gotags

let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
\ }
"}}}
"}}}
