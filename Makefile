.PHONY: all dep test lint build install run

PKG := "./..."

all: build

lint: dep ## Lint the files
	@go get golang.org/x/lint/golint
	@golint -set_exit_status ${PKG}

test: ## Run unittests
	@go test -v ${PKG}

dep: ## Get the dependencies
	@go get -v -d ${PKG}

build: dep ## Build the binary file
	@go build -i -v ${PKG}

install: dep ## Build the binary file
	@go install ${PKG}

run: ## Run application
	@go run -v ${PKG}
